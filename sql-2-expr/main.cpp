#include "shdb/db.h"
#include "shdb/sql.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void test_expression()
{
    auto sql = shdb::Sql(nullptr);

    auto check = [&](const std::string &query, const shdb::Row &expected) {
        auto result = sql.execute(query);
        assert(result.rows.size() == 1 && *result.rows[0] == expected);
    };

    check("SELECT 11", {static_cast<uint64_t>(11)});
    check("SELECT 11+11", {static_cast<uint64_t>(22)});
    check("SELECT 2*2", {static_cast<uint64_t>(4)});
    check("SELECT 1 > 0", {true});
    check("SELECT (50-30)*2 <= 1*2*3*4", {false});
    check("SELECT \"Hi\"", {std::string("Hi")});
    check(
        "SELECT \"Mike\", \"Bob\", 1+2, 1>0",
        {std::string("Mike"), std::string("Bob"), static_cast<uint64_t>(3), true});

    std::cout << "Test expression passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect("./mydb", 1);
    auto sql = shdb::Sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql.execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_expression();
}
