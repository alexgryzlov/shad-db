#include "table.h"
#include "schema.h"
#include "ast.h"
#include "lexer.h"
#include "eval.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <chrono>
#include <random>
#include <cassert>

using namespace shdb;

std::shared_ptr<Schema> generate_schema(int n)
{
    auto schema = std::make_shared<Schema>();
    if (n < 26) {
        for (int i = 0; i < n; ++i) {
            schema->push_back({.name = std::string(1, 'a' + i)});
        }
    } else {
        for (int i = 0; i < n; ++i) {
            schema->push_back({.name = std::string("c") + std::to_string(i)});
        }
    }
    return schema;
}

std::shared_ptr<Table> generate_table(std::shared_ptr<Schema> schema, size_t rows)
{
    auto table = std::make_shared<Table>();
    table->columns.resize(schema->size());

    std::mt19937 gen;
    gen.seed(0);

    for (size_t column = 0; column < schema->size(); ++column) {
        table->columns[column].reserve(rows);
        for (size_t row = 0; row < rows; ++row) {
            table->columns[column].push_back(gen());
        }
    }

    return table;
}

std::shared_ptr<Select> parse(const std::string& query)
{
    Lexer lexer(query.c_str(), query.c_str() + query.size());
    std::shared_ptr<shdb::Ast> result;
    std::string error;
    shdb::Parser parser(lexer, result, error);
    parser.parse();
    if (!result || !error.empty()) {
        std::cout << "Bad input: " << error << std::endl;
        return nullptr;
    }
    if (result->type != Type::select) {
        std::cout << "Bad input: not a select expression" << std::endl;
        return nullptr;
    }
    return std::static_pointer_cast<Select>(result);
}

std::shared_ptr<Table> evaluate(std::shared_ptr<Schema> schema, std::shared_ptr<Table> table, std::shared_ptr<Select> select)
{
    Eval eval(schema, table, select);
    return eval.run();
}

void print_table(std::shared_ptr<Table> table)
{
    for (size_t row = 0; row < table->columns[0].size(); ++row) {
        for (auto& column : table->columns) {
            std::cout << column[row] << " ";
        }
        std::cout << std::endl;
    }
}

std::shared_ptr<Schema> load_schema(std::ifstream& in)
{
    auto schema = std::make_shared<Schema>();
    std::string line;
    std::getline(in, line);
    std::stringstream stream(line);
    std::string name;
    while (stream >> name) {
        schema->push_back({.name = name});
    }
    return schema;
}

std::shared_ptr<Table> load_table(std::shared_ptr<Schema> schema, std::ifstream& in)
{
    auto table = std::make_shared<Table>();
    table->columns.resize(schema->size());

    std::string line;
    while (!in.eof()) {
        std::getline(in, line);
        if (line.empty()) {
            break;
        }
        std::stringstream stream(line);
        int value;
        for (size_t index = 0; index < table->columns.size(); ++index) {
            if (stream >> value) {
                table->columns[index].push_back(value);
            } else {
                table->columns[index].push_back(0);
            }
        }
        line.clear();
    }
    return table;
}

void interactive(std::shared_ptr<Schema> schema, std::shared_ptr<Table> table)
{
    while (!std::cin.eof()) {
        std::cout << "> ";
        std::string query;
        std::getline(std::cin, query);
        if (query.empty()) {
            continue;
        }
        if (auto select = parse(query)) {
            auto start = std::chrono::high_resolution_clock::now();
            auto result = evaluate(schema, table, select);
            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Executed in " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns" << std::endl;
            print_table(result);
        }
    }
}

void assert_tables_equal(std::shared_ptr<Table> lhs, std::shared_ptr<Table> rhs)
{
    assert(lhs->columns.size() == rhs->columns.size());
    for (size_t i = 0; i < lhs->columns.size(); ++i) {
        assert(lhs->columns[i] == rhs->columns[i]);
    } 
}

void test_simple()
{
    auto table = std::make_shared<Table>(Table{.columns = {{1},{2},{3}}});
    auto schema = std::make_shared<Schema>(Schema{{"a"}, {"b"}, {"c"}});

    auto test = [&] (const std::string& query, Table expected) {
        auto result = evaluate(schema, table, parse(query));
        assert_tables_equal(result, std::make_shared<Table>(std::move(expected)));
    };

    test("SELECT a", {.columns = {{1}}});
    test("SELECT b", {.columns = {{2}}});
    test("SELECT a, c", {.columns = {{1}, {3}}});
    test("SELECT sum(a) + sum(c)", {.columns = {{4}}});
    test("SELECT min(b) + max(c) WHERE a > 0", {.columns = {{5}}});

    std::cout << "Test simple passed" << std::endl;
}

void test_basic()
{
    auto table = std::make_shared<Table>(Table{.columns = {{-1,1},{-2,2},{-3,3}}});
    auto schema = std::make_shared<Schema>(Schema{{"a"}, {"b"}, {"c"}});

    auto test = [&] (const std::string& query, Table expected) {
        auto result = evaluate(schema, table, parse(query));
        assert_tables_equal(result, std::make_shared<Table>(std::move(expected)));
    };

    test("SELECT a", {.columns = {{-1,1}}});
    test("SELECT - a", {.columns = {{1,-1}}});
    test("SELECT a + 1", {.columns = {{0,2}}});
    test("SELECT a - 1", {.columns = {{-2,0}}});
    test("SELECT a * 2", {.columns = {{-2,2}}});
    test("SELECT b / 2", {.columns = {{-1,1}}});
    test("SELECT a + b", {.columns = {{-3,3}}});
    test("SELECT a - b", {.columns = {{1,-1}}});
    test("SELECT b * c", {.columns = {{6,6}}});
    test("SELECT c / b", {.columns = {{1,1}}});
    test("SELECT a WHERE a > -1", {.columns = {{1}}});
    test("SELECT a WHERE a >= -1", {.columns = {{-1, 1}}});
    test("SELECT a WHERE a = -1", {.columns = {{-1}}});
    test("SELECT a WHERE a < 1", {.columns = {{-1}}});
    test("SELECT a WHERE a <= 1", {.columns = {{-1, 1}}});
    test("SELECT a WHERE a < b", {.columns = {{1}}});
    test("SELECT a WHERE a > b", {.columns = {{-1}}});
    test("SELECT a WHERE a <= b", {.columns = {{1}}});
    test("SELECT a WHERE a >= b", {.columns = {{-1}}});
    test("SELECT a WHERE a = b", {.columns = {{}}});
    test("SELECT max(a), min(b), sum(c), avg(c)", {.columns = {{1}, {-2}, {0}, {0}}});
    test("SELECT min(a - c)", {.columns = {{-2}}});
    test("SELECT max(a + c)", {.columns = {{4}}});
    test("SELECT sum(a) + sum(c) WHERE a > 0", {.columns = {{4}}});
    test("SELECT sum(a) + sum(c) WHERE a > 0", {.columns = {{4}}});
    test("SELECT max(b) + min(b) WHERE a > 0 AND b > 0", {.columns = {{4}}});
    test("SELECT -(max(b) - min(c)) WHERE a > 0 OR b < 0", {.columns = {{-5}}});
    test("SELECT b, a", {.columns = {{-2,2}, {-1,1}}});

    std::cout << "Test basic passed" << std::endl;
}

std::shared_ptr<Table> canonical_sum_a(std::shared_ptr<Table> table)
{
    int sum = 0;
    for (auto v : table->columns[0]) {
        sum += v;
    }
    return std::make_shared<Table>(Table{.columns = {{sum}}});
}

std::shared_ptr<Table> canonical_sum_a_b(std::shared_ptr<Table> table)
{
    int sum = 0;
    for (size_t i = 0; i < table->columns[0].size(); ++i) {
        sum += table->columns[0][i] - table->columns[1][i];
    }
    return std::make_shared<Table>(Table{.columns = {{sum}}});
}

std::shared_ptr<Table> canonical_max_a_b(std::shared_ptr<Table> table)
{
    int max = std::numeric_limits<int>::min();
    for (size_t i = 0; i < table->columns[0].size(); ++i) {
        if (table->columns[1][i] > 1000) {
            max = std::max(max, table->columns[0][i]);
        }
    }
    return std::make_shared<Table>(Table{.columns = {{max}}});
}

void test_generate()
{
    auto schema = generate_schema(5);
    auto table = generate_table(schema, 1000000);

    auto test = [&] (const std::string& query, std::shared_ptr<Table> (*canonical) (std::shared_ptr<Table>)) {
        auto select = parse(query);

        auto start = std::chrono::high_resolution_clock::now();
        auto result = evaluate(schema, table, select);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();

        start = std::chrono::high_resolution_clock::now();
        auto expected = canonical(table);
        end = std::chrono::high_resolution_clock::now();
        auto baseline = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();

        assert_tables_equal(result, expected);
        std::cout << query << " " << (float) elapsed / baseline << std::endl;
        assert(elapsed/5 < baseline);
    };

    test("SELECT sum(a)", canonical_sum_a);
    test("SELECT sum(a-b)", canonical_sum_a_b);
    test("SELECT max(a) WHERE b > 1000", canonical_max_a_b);

    std::cout << "Test generate passed" << std::endl;
}

void interactive_load(int argc, char *argv[])
{
    if (argc != 2) {
        std::cout << "Usage: columnar tablefile" << std::endl;
        return;
    }

    std::ifstream in(argv[1]);
    auto schema = load_schema(in);
    auto table = load_table(schema, in);
    print_table(table);
    interactive(schema, table);
}

void interactive_generate(int argc, char *argv[])
{
    if (argc != 3) {
        std::cout << "Usage: columnar ncols nrows" << std::endl;
        return;
    }

    int columns = atoi(argv[1]);
    int rows = atoi(argv[2]);

    auto schema = generate_schema(columns);
    auto table = generate_table(schema, rows);
    interactive(schema, table);
}

int main(int argc, char *argv[])
{
    test_simple();
    test_basic();
    test_generate();

    //interactive_load(argc, argv);
    //interactive_generate(argc, argv);
}
