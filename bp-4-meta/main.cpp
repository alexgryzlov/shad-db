#include "shdb/db.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void test_metadata_prepare()
{
    shdb::PageIndex pool_size = 5;
    auto db = create_database(pool_size);
    auto table = db->get_table("test_table", fixed_schema);

    std::vector<std::pair<shdb::RowId, shdb::Row>> rows;
    shdb::PageIndex page_count = 0;
    uint64_t row_count = 0;

    while (row_count < 10) {
        std::stringstream stream;
        stream << "clone" << row_count;
        auto row = shdb::Row{row_count, stream.str(), 20UL + row_count % 10, row_count % 10 > 5};
        auto row_id = table->insert_row(row);
        rows.emplace_back(row_id, std::move(row));
        page_count = std::max(page_count, row_id.page_index + 1);
        ++row_count;
    }
}

void test_metadata_validate()
{
    auto db = shdb::connect("./mydb", 1);
    auto table = db->get_table("test_table");
    uint64_t row_count = 0;
    for (auto row : shdb::Scan(table)) {
        if (!row.empty()) {
            std::stringstream stream;
            stream << "clone" << row_count;
            auto expected = shdb::Row{row_count, stream.str(), 20UL + row_count % 10, row_count % 10 > 5};
            std::cout << shdb::to_string(row) << std::endl;
            assert(row == expected);
            ++row_count;
        }
    }
    assert(row_count == 10);
}

void test_metadata(int argc, char *argv[])
{
    if (argc < 2) {
        test_metadata_prepare();
        std::cout << "Validating metadata in the same process:" << std::endl;
        test_metadata_validate();
        if (int child = fork(); child > 0) {
            int wstatus;
            int pid = waitpid(child, &wstatus, 0);
            if (pid == -1) {
                perror("waitpid failed");
                throw "waitpid failed";
            }
            assert(WEXITSTATUS(wstatus) == 0);
        } else if (child == 0) {
            if (execl(argv[0], argv[0], "validate", NULL) == -1) {
                perror("exec failed");
                throw "exec failed";
            }
        } else {
            perror("fork failed");
            throw "fork failed";
        }
    } else {
        std::cout << "Validating metadata in child:" << std::endl;
        test_metadata_validate();
        exit(0);
    }
    std::cout << "Test metadata passed" << std::endl;
}


int main(int argc, char *argv[])
{
    test_metadata(argc, argv);
}
