#pragma once

#include "page.h"

#include <filesystem>

namespace shdb {

class File
{
    int fd = -1;
    off_t size;

    void read(void *buf, size_t count, off_t offset) const;
    void write(void *buf, size_t count, off_t offset) const;
    void alloc(off_t len);
    void safe_syscall(size_t count, const std::function<ssize_t()> &call) const;
    size_t discover_size() const;

public:
    File(const std::filesystem::path &path, bool create);
    ~File();

    int get_fd() const;
    off_t get_size() const;
    off_t get_page_count() const;
    void read_page(void *buf, PageIndex index) const;
    void write_page(void *buf, PageIndex index) const;
    PageIndex alloc_page();
};

}    // namespace shdb
