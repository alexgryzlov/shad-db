#pragma once

#include <vector>

namespace shdb {

using Column = std::vector<int>;

struct Table {
    std::vector<Column> columns;
};

}    // namespace shdb
