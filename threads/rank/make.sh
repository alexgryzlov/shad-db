#!/bin/bash

set -e
set -x

source usernames.sh

mkdir -p bins

for username in ${usernames[@]}
do
  cp ${username}/threads/main.cpp shad-db/threads/main.cpp
  make -C shad-db/threads main
  cp shad-db/threads/main bins/${username}
done
