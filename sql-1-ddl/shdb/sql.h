#pragma once

#include "ast.h"
#include "database.h"
#include "rowset.h"

namespace shdb {

class Sql
{
    // Your code goes here

public:
    explicit Sql(std::shared_ptr<Database> db);

    Rowset execute(const std::string &query);
};

}    // namespace shdb
