#pragma once

#include "schema.h"

#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace shdb {

enum class Type {
    name,
    string,
    number,
    binary,
    unary,
    list,
    function,
    select,
};

enum class Opcode {
    plus,
    minus,
    mul,
    div,
    eq,
    ne,
    lt,
    le,
    gt,
    ge,
    land,
    lor,
    lnot,
    uminus,
};


struct Ast
{
    Type type;

    Ast(Type type);
    virtual ~Ast() = default;
};

struct String : public Ast
{
    std::string value;

    String(Type type, std::string value);
};

struct Number : public Ast
{
    int value;

    Number(int value);
};

struct Binary : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> lhs;
    std::shared_ptr<Ast> rhs;

    Binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs);
};

struct Unary : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> operand;

    Unary(Opcode op, std::shared_ptr<Ast> operand);
};

struct List : public Ast
{
    std::vector<std::shared_ptr<Ast>> list;

    List();
    List(std::shared_ptr<Ast> element);
    void append(std::shared_ptr<Ast> element);
};

struct Function : public Ast
{
    std::string name;
    std::shared_ptr<List> args;

    Function(std::string name, std::shared_ptr<List> args);
};

struct Select : public Ast
{
    std::shared_ptr<List> list;
    std::shared_ptr<Ast> where;

    Select(
        std::shared_ptr<List> list,
        std::shared_ptr<Ast> where);
};

std::shared_ptr<Ast> new_name(std::string value);
std::shared_ptr<Ast> new_string(std::string value);
std::shared_ptr<Ast> new_number(int value);
std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs);
std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand);
std::shared_ptr<List> new_list();
std::shared_ptr<List> new_list(std::shared_ptr<Ast> element);
std::shared_ptr<Ast> new_function(std::string name, std::shared_ptr<List> args);
std::shared_ptr<Ast> new_select(
    std::shared_ptr<List> list,
    std::shared_ptr<Ast> where);

std::string to_string(std::shared_ptr<Ast> ast);

}    // namespace shdb
