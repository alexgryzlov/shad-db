#pragma once

#include <cstdint>
#include <vector>

#include "discovery.h"
#include "env.h"
#include "retrier.h"
#include "server_transaction.h"
#include "storage.h"

// Class Server is an actor that implements single-node key-value store.
//
// Requests to the node are sent via Env, so the client must be an actor too.
//
// The Server communicates with other nodes via the Env object, by sending
// and receiving messages.
class Server : public IActor {
public:
  // Initialize Server, as a member of cluster with N nodes.
  // key_intervals.size() == N, each interval is a range of keys owned by the
  // corresponding node
  // 0 <= id < N, the id of this node.
  // intervals[i + 1].key_begin = intervals[i].key_end
  // intervals[0].key_begin = 0
  // intervals[N - 1].key_end = max key
  Server(ActorId id, std::vector<KeyInterval> key_intervals, EnvProxy env);

  // Actor interface implementation.
  virtual ActorId get_id() const override;
  virtual void on_tick(Clock &clock, std::vector<Message> messages) override;

  // Internal key/value storage. Exposed for scenario testing.
  const Storage &storage() const { return storage_; }

private:
  const ActorId id_;
  const std::vector<KeyInterval> key_intervals_;
  EnvProxy env_;

  Storage storage_;
  RetrierExpBackoff retrier_;

  std::unordered_map<TransactionId, ServerTransaction> transactions_;

  TransactionId get_message_txid(const Message &msg) const;
  void report_unexpected_message(const Message &msg) const;
  ServerTransaction *get_or_create_transaction(TransactionId txid);
};
