#!/bin/bash

set -e

source usernames.sh

for username in ${usernames[@]}
do
cd ${username}
git log HEAD~1..HEAD
cd ..
done
