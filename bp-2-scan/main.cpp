#include "shdb/db.h"

#include <cassert>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void test_scan()
{
    shdb::PageIndex pool_size = 5;
    auto db = create_database(pool_size);
    auto table = db->get_table("test_table", fixed_schema);

    std::vector<std::pair<shdb::RowId, shdb::Row>> rows;
    shdb::PageIndex page_count = 0;
    uint64_t row_count = 0;

    while (page_count < 2 * pool_size) {
        std::stringstream stream;
        stream << "clone" << row_count;
        auto row = shdb::Row{row_count, stream.str(), 20UL + row_count % 10, row_count % 10 > 5};
        auto row_id = table->insert_row(row);
        std::cout << "inserted " << shdb::to_string(row) << " page index " << row_id.page_index << " row index "
                  << row_id.row_index << std::endl;
        rows.emplace_back(row_id, std::move(row));
        page_count = std::max(page_count, row_id.page_index + 1);
        ++row_count;
    }

    std::cout << "Reading rows:" << std::endl;
    size_t index = 0;
    for (auto row : shdb::Scan(table)) {
        if (!row.empty()) {
            std::cout << shdb::to_string(row) << std::endl;
            assert(row == rows[index].second);
            ++index;
        }
    }
    assert(index == rows.size());

    index = 0;
    auto scan = shdb::Scan(table);
    for (auto it = scan.begin(), end = scan.end(); it != end; ++it) {
        auto row = it.get_row();
        if (!row.empty()) {
            assert(row == rows[index].second);
            assert(it.get_row_id() == rows[index].first);
            ++index;
        }
    }
    assert(index == rows.size());

    std::cout << "Test scan passed" << std::endl;
}


int main(int argc, char *argv[])
{
    test_scan();
}
